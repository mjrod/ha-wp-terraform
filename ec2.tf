# Configure compute resources
resource "aws_instance" "ha_wp_ec2_instance" {
  ami = "ami-0231217be14a6f3ba" #us-east-2, amazon linux 5.10
  instance_type = "t2.micro"
  associate_public_ip_address = true
  subnet_id = aws_subnet.ha_wp_public_subnet.id
  vpc_security_group_ids = [aws_security_group.ha_wp_public_subnet_security_group.id]
  key_name = aws_key_pair.ha_wp_key_pair.id
  # user_data = "${file("install-ghost.sh")}"

  tags = {
    "Name" = "ha_wp_ec2_instance",
    "env" = "test"
  }
}

output "ha_wp_ec2_instance_ip" {
  value = aws_instance.ha_wp_ec2_instance.public_ip
}