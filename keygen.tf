# Configure SSH Key Pair
resource "tls_private_key" "ssh_key_pair" {
  algorithm = "RSA"
  rsa_bits = 4096
}
resource "aws_key_pair" "ha_wp_key_pair" {
  key_name = "ha_wp_key_pair"
  public_key = tls_private_key.ssh_key_pair.public_key_openssh

  provisioner "local-exec" {
    command = "echo '${tls_private_key.ssh_key_pair.private_key_pem}' > ./ha_wp_key_pair.pem"
  }
}