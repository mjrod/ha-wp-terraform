# Configure VPC resource
resource "aws_vpc" "ha_wp_vpc" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true
  tags = {
    "Name" = "ha_wp_vpc",
    "env" = "test"
  }
}

# Configure internet gateway resource
resource "aws_internet_gateway" "ha_wp_igw" {
  vpc_id = aws_vpc.ha_wp_vpc.id
  tags = {
    "Name" = "ha_wp_igw"
    "env" = "test"
  }
}

# Configure route table to allow igw to public subnet communication
resource "aws_route_table" "ha_wp_route_table" {
  vpc_id = aws_vpc.ha_wp_vpc.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.ha_wp_igw.id
  }
  tags = {
    "Name" = "ha_wp_route_table"
    "env" = "test"
  }
}
resource "aws_route_table_association" "ha_wp_public_rtb_association" {
  subnet_id = aws_subnet.ha_wp_public_subnet.id
  route_table_id = aws_route_table.ha_wp_route_table.id
}

# Configure public subnet resource
resource "aws_subnet" "ha_wp_public_subnet" {
  vpc_id = aws_vpc.ha_wp_vpc.id
  cidr_block = "10.0.1.0/24"
  tags = {
    "Name" = "ha_wp_public_subnet"
    "env" = "test"
  }
}
